/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Courses;

import Business.UserAccount.UserAccount;

/**
 *
 * @author Xie
 */
public class Course {
    private int courseId;
    private String courseName;
    private String courseDay;
    private int courseStartTime;
//    private int courseEndTime;
    private UserAccount teacher;
    private int availSeats;
    private static int count = 1;
    private String description;
    
    public Course(String courseName, String courseDay, int courseStartTime, UserAccount teacher, String description){
        this.courseId = count + courseName.length();
        count++;
//        this.courseEndTime = courseEndTime;
        this.courseName = courseName;
        this.courseStartTime = courseStartTime;
        this.teacher = teacher;    
        this.description = description;
        this.courseDay = courseDay;
        this.availSeats = 40;
    }

    public String getCourseDay() {
        return courseDay;
    }

    public void setCourseDay(String courseDay) {
        this.courseDay = courseDay;
    }

    
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    

    public int getAvailSeats() {
        return availSeats;
    }

    public void setAvailSeats(int availSeats) {
        this.availSeats = availSeats;
    }

    public void registerCourse(){
        if(this.availSeats > 0)  
            this.availSeats--;
    }
    
    public void dropCourse(){
        if(this.availSeats < 40)  
            this.availSeats++;
    }
    
    public int getCourseId() {
        return courseId;
    }

    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public int getCourseStartTime() {
        return courseStartTime;
    }

    public void setCourseStartTime(int courseStartTime) {
        this.courseStartTime = courseStartTime;
    }

//    public int getCourseEndTime() {
//        return courseEndTime;
//    }
//
//    public void setCourseEndTime(int courseEndTime) {
//        this.courseEndTime = courseEndTime;
//    }

    public UserAccount getTeacher() {
        return teacher;
    }

    public void setTeacher(UserAccount teacher) {
        this.teacher = teacher;
    }
    
    @Override
    public String toString() {
        return courseName;
    }
}
