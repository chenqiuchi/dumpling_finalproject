/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Role.HumanResourcesRole;
import Business.Role.Role;
import Business.WorkQueue.WorkQueue;

import java.util.ArrayList;

/**
 *
 * @author Xie
 */
public class HumanResourcesOrganization extends Organization{
    private WorkQueue commentWorkQueue;
    
    public HumanResourcesOrganization() {
        super(Organization.Type.HumanResources.getValue());
        
        
        commentWorkQueue = new WorkQueue();
    }

    public WorkQueue getCommentWorkQueue() {
        return commentWorkQueue;
    }

    public void setCommentWorkQueue(WorkQueue commentWorkQueue) {
        this.commentWorkQueue = commentWorkQueue;
    }
    
    
    
    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList();
        roles.add(new HumanResourcesRole());
        return roles;
    }
}
