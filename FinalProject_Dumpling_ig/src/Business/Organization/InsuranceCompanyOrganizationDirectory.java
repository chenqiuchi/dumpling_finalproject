/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import java.util.ArrayList;

/**
 *
 * @author Xie
 */
public class InsuranceCompanyOrganizationDirectory extends OrganizationDirectory{
    private ArrayList<Organization> organizationList;

    public InsuranceCompanyOrganizationDirectory() {
        organizationList = new ArrayList();
    }

    public ArrayList<Organization> getOrganizationList() {
        return organizationList;
    }
    public Organization createOrganization(Organization.Type type){
        Organization organization = null;
        if (type.getValue().equals(Organization.Type.Sales.getValue())){
            organization = new SalesOrganization();
            organizationList.add(organization);
        }
        return organization;
    }
    
    public void deleteOrganization(Organization.Type type){
        for(Organization o: this.organizationList ){
            if(o.getName().equals(type.getValue())){
                organizationList.remove(o);
                break;
            }
        }
    }
}
