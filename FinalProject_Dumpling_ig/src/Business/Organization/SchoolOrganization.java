/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author Xie
 */
public abstract class SchoolOrganization extends Organization{

    public SchoolOrganization(String name) {
        super(name);
    }
    
    public enum Type{
        Admin("Admin Organization"),
        HumanResources("Human Resource Department"),
        CourseStatistics("Course Statistics Department"), 
        Sales("Sales Department"), 
        Teachers("Teacher Organization"), 
        Students("Student Organization");
       
        
        
        
        private String value;
        private Type(String value) {
            this.value = value;
        }
        public String getValue() {
            return value;
        }
    }
    
    public abstract ArrayList<Role> getSupportedRole();
    
}
