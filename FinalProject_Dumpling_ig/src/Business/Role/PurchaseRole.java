/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Organization.Organization;
import Business.Organization.PurchaseOrganization;
import Business.Organization.TeachersOrganization;
import Business.UserAccount.UserAccount;
import Interface.Purchase.PurchaseWorkArea;
import Interface.Teachers.TeacherCatalogJPanel;
import javax.swing.JPanel;

/**
 *
 * @author Xie
 */
public class PurchaseRole extends Role{
    @Override
    public JPanel createWorkArea(JPanel righJPanel, UserAccount account, Organization organization, Enterprise enterprise, EcoSystem business) {
        return new PurchaseWorkArea(righJPanel, account, (PurchaseOrganization)organization, enterprise, business);
    }
}
