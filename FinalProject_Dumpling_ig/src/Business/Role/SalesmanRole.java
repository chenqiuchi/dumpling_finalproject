/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Organization.Organization;
import Business.Organization.SalesOrganization;
import Business.UserAccount.UserAccount;
import Interface.Sale.SalesWorkArea;
import javax.swing.JPanel;

/**
 *
 * @author Xie
 */
public class SalesmanRole extends Role{
    @Override
    public JPanel createWorkArea(JPanel rightJPanel, UserAccount account, Organization organization, Enterprise enterprise, EcoSystem business) {
        return new SalesWorkArea(rightJPanel, account, (SalesOrganization)organization, enterprise, business);
    }
}
