/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.UserAccount;

import Business.Employee.Employee;
import Business.Role.Role;
import Business.WorkQueue.WorkQueue;

/**
 *
 * @author Xie
 */
public class UserAccount {
    private String username;
    private String password;
    private Employee employee;
    private Role role;
    private WorkQueue workQueue;
    private WorkQueue courseQueue;
    private WorkQueue commentQueue;
    private WorkQueue orderQueue;
    private int salary;
    
    
    public UserAccount() {
        workQueue = new WorkQueue();
        courseQueue = new WorkQueue();
        commentQueue = new WorkQueue();
        orderQueue = new WorkQueue();
        
    }

    public WorkQueue getOrderQueue() {
        return orderQueue;
    }

    public void setOrderQueue(WorkQueue orderQueue) {
        this.orderQueue = orderQueue;
    }

    public WorkQueue getCourseQueue() {
        return courseQueue;
    }

    public void setCourseQueue(WorkQueue courseQueue) {
        this.courseQueue = courseQueue;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public WorkQueue getCommentQueue() {
        return commentQueue;
    }

    public void setCommentQueue(WorkQueue commentQueue) {
        this.commentQueue = commentQueue;
    }
    
    
    
    
    
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Employee getEmployee() {
        return employee;
    }

    public WorkQueue getWorkQueue() {
        return workQueue;
    }

    
    
    @Override
    public String toString() {
        return username;
    }
}
