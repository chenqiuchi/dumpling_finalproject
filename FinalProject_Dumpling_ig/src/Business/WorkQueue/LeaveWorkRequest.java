/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

/**
 *
 * @author Xie
 */
public class LeaveWorkRequest extends WorkRequest{
    private String DenyReason;
    private String Type;
    private String StartDate;
    private int duration;

    public String getDenyReason() {
        return DenyReason;
    }

    public void setDenyReason(String DenyReason) {
        this.DenyReason = DenyReason;
    }

    public String getType() {
        return Type;
    }

    public void setType(String Type) {
        this.Type = Type;
    }

    public String getStartDate() {
        return StartDate;
    }

    public void setStartDate(String StartDate) {
        this.StartDate = StartDate;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }
    

    
}
